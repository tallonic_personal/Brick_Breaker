﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {
    public GameObject pause;
    public GameObject BallPrefab;
    public static bool paused = false;

    public void LoadMenu () {
        LoadScene ("Menu");
    }
    public void LoadLevelSelect () {
        if (!PlayerPrefs.HasKey ("Pers_Level")) {
            PlayerPrefs.SetInt ("Pers_Level", 1);
        }
        LoadScene ("Level_Select");
    }
    public void LoadNextLevel (int level = 1) {
        PlayerPrefs.SetInt ("Level", level);
        PlayerPrefs.Save ();
        LoadScene ("Level");
    }
    public void LoadCongratulations () {
        LoadScene ("Congratulations");
    }
    public void Quit () {
        Application.Quit ();
    }

    public void CheckBricksDestroyed () {
        if (Brick.BricksCount <= 0) {
            StartCoroutine (Fanfare ());
        }
    }

    private void LoadScene(string sceneName)
    {
        UIFade.Instance.FadeOut(() =>
        {
            var operation = SceneManager.LoadSceneAsync (sceneName);
            operation.completed += _ => UIFade.Instance.FadeIn();
        });
    }

    IEnumerator Fanfare () {
        GameObject.Find ("ball(Clone)").GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
        GameObject.Find ("paddle_player").GetComponent<PlayerController> ().MoveSpeed = 0;
        var audioController = GameObject.Find ("audioController").GetComponent<AudioController> ();
        audioController.StopSound ();

        yield return new WaitForSeconds (1f);
        audioController.PlayFanfare ();
        yield return new WaitForSeconds (1f);
        
        var fanfarePanel = GameObject.FindObjectOfType<FanfarePanelAnimation>(true);
        fanfarePanel.gameObject.SetActive(true);
        
        yield return new WaitForSeconds (5f);

        var level = PlayerPrefs.GetInt("Level");
        var lastLevel = PlayerPrefs.GetInt("Pers_Level");
        
        if (level < 5)
        {
            var newLevel = level + 1;
            if (newLevel > lastLevel)
            {
                Debug.Log("new level " + newLevel);
                PlayerPrefs.SetInt("Pers_Level", newLevel);
                PlayerPrefs.Save();
                LoadNextLevel(newLevel);
            }
            else
            {
                LoadScene ("Level_Select");
            }
        } else {
            PlayerPrefs.SetInt ("Pers_Level", 6);
            PlayerPrefs.Save ();
            LoadCongratulations ();
        }
    }

    void Update () {
        if (Input.GetKeyDown (KeyCode.Escape)) {
            if (!paused) {
                Time.timeScale = 0;
                pause.SetActive (true);
                paused = true;
            } else {
                Time.timeScale = 1;
                pause.SetActive (false);
                paused = false;
            }
        }
        if (Input.GetKeyDown (KeyCode.E)) {
            if (paused) {
                LoadLevelSelect ();
            }
        }
    }
}