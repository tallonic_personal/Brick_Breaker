using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class FanfarePanelAnimation : MonoBehaviour
{
    [SerializeField] private Transform fanfarePanel;
    [SerializeField] private Transform congratulationsText;
    [SerializeField] private Transform levelCompletedText;
    
    IEnumerator Start()
    {
        fanfarePanel.localScale = Vector3.zero;
        congratulationsText.localScale = Vector3.zero;
        levelCompletedText.localScale = Vector3.zero;
        
        var sequence = DOTween.Sequence()
            .Append(fanfarePanel.DOScale(Vector3.one, .5f))
            .Append(congratulationsText.DOScale(Vector3.one, .5f))
            .Join(levelCompletedText.DOScale(Vector3.one, .5f));
        
        yield return sequence.WaitForCompletion();
    }
}
