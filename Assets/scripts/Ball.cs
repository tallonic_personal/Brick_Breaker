﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public float Speed = 10f;
    public AudioClip Hit;

    private PlayerController Player;

    Vector3 _positionToPlayer;
    bool hasStarted = false;

    AudioSource _hitSound;
    SceneController _sceneController;
    BallRespawn _respawn;

    void Start()
    {
        Player = FindObjectOfType<PlayerController>();
        _positionToPlayer = transform.position - Player.transform.position;
        _hitSound = GetComponent<AudioSource>();
        _sceneController = FindObjectOfType<SceneController>();
        _respawn = FindObjectOfType<BallRespawn>();

        Speed += PlayerPrefs.GetInt("Level") / 3;
    }

    void Update()
    {
        if (!hasStarted)
        {
            transform.position = Player.transform.position + _positionToPlayer;

            if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, 1).normalized * Speed;
                //transform.GetComponent<Rigidbody2D> ().velocity = new Vector2 (Random.Range (-(Speed / 3), Speed / 3), Speed);
                hasStarted = true;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Deadline")
        {
            _respawn.DestroyBall(gameObject);
            return;
        }


        if (hasStarted)
        {
            var rb2D = GetComponent<Rigidbody2D>();

            if (collision.gameObject.TryGetComponent(out PlayerController playerController))
            {
                var newDir = transform.position.x > playerController.transform.position.x
                    ? new Vector2(1f, 1f)
                    : new Vector2(-1, 1);

                var newSpeed = newDir.normalized * Speed;
                rb2D.velocity = newSpeed;

                _hitSound.Play();
            }
            else if (collision.gameObject.TryGetComponent(out Brick brick))
            {
                var newSpeed = rb2D.velocity;
                var minimumYSpeed = Speed / 1.5f;
                if (rb2D.velocity.y < 0f)
                    minimumYSpeed = -minimumYSpeed;

                rb2D.velocity = new Vector2(newSpeed.x, Mathf.Max(newSpeed.y, minimumYSpeed));

                _hitSound.PlayOneShot(Hit);
            }
            else
            {
                _hitSound.PlayOneShot(Hit);
            }
        }
    }
}