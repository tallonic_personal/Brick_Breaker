using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LevelSelectButtonSpriteHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Button levelButton;
    [SerializeField] private Image spriteObject;
    [SerializeField] private Image highlightObject;
    
    [Header("Label")]
    [SerializeField] private Image stageTextImage;
    [SerializeField] private Image stageNumberImage;
    [SerializeField] private Image currentLevelImage;
    [SerializeField] private Sprite stageTextSprite;

    [SerializeField] private int level;

    private void Start()
    {
        var sceneController = FindObjectOfType<SceneController>();
        
        var playerLevel = PlayerPrefs.GetInt ("Pers_Level");
        if (level <= playerLevel)
        {
            levelButton.onClick.AddListener(() => sceneController.LoadNextLevel(level));
        }
        else
        {
            var color  = new Color(.5f,.5f,.5f,.5f);
            spriteObject.color = color;
            currentLevelImage.enabled = false;
            enabled = false;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        highlightObject.enabled = true;
        stageTextImage.sprite = stageTextSprite;
        stageNumberImage.sprite = currentLevelImage.sprite;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        highlightObject.enabled = false;
    }
}
