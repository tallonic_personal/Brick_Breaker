﻿using System.Collections;
using TMPro;
using UnityEngine;

public class CongratulationsController : MonoBehaviour {
    [SerializeField] private TextMeshProUGUI Congratulations;
    [SerializeField] private TextMeshProUGUI thanks;

    void Start () {
        StartCoroutine ("ShowCongratulations");
    }

    IEnumerator ShowCongratulations ()
    {
        yield return new WaitForSeconds(2f);
        
        while (Congratulations.color.a < 0.99f) {
            var cColor = Congratulations.color;
            Congratulations.color = new Color (cColor.r, cColor.g, cColor.b, Mathf.Clamp01 (cColor.a + 0.01f));
            yield return 0;
        }

        yield return new WaitForSeconds(1f);
        
        while (thanks.color.a < 0.99f) {
            var cColor = thanks.color;
            thanks.color = new Color (cColor.r, cColor.g, cColor.b, Mathf.Clamp01 (cColor.a + 0.01f));
            yield return 0;
        }
        
        yield return new WaitForSeconds (5f);
        var sceneController = GameObject.Find ("SceneController").GetComponent<SceneController> ();
        sceneController.LoadMenu ();
    }
}