using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIFade : MonoBehaviour
{
    private static UIFade _instance;
    public static UIFade Instance => _instance;
    
    public Image imageToFade;
    public float fadeDuration = 1f;

    private void Start()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
            return;
        }

        _instance = this;
    }

    public void Fade(float startIn, float endIn, Action callback = null) => StartCoroutine(FadeImage(startIn,endIn, callback));
   
    public void FadeOut(Action callback = null) => StartCoroutine(FadeImage(0f,1f, callback));
    
    public void FadeIn(Action callback = null) => StartCoroutine(FadeImage(1f,0f, callback));

    private IEnumerator FadeImage(float startIn, float endIn, Action callback)
    {
        imageToFade.color = new Color(imageToFade.color.r, imageToFade.color.g, imageToFade.color.b, imageToFade.color.a);

        float t = 0;
        while (t < fadeDuration)
        {
            float alpha = Mathf.Lerp(startIn, endIn, t / fadeDuration);
            imageToFade.color = new Color(imageToFade.color.r, imageToFade.color.g, imageToFade.color.b, alpha);
            t += Time.deltaTime;
            yield return null;
        }
        
        callback?.Invoke();
    }
}