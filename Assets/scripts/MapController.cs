﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class MapController : MonoBehaviour
{
    public ColorToPrefab[] Mapping;
    public int debug_level = 1;
    public bool enableDebug;

    private Transform _map;
    private Texture2D _levelSprite;

    private void Awake()
    {
        if (enableDebug)
        {
            PlayerPrefs.SetInt("Level", debug_level);
            PlayerPrefs.SetInt("Pers_Level", debug_level);
        }
    }

    void Start()
    {
        //_levelSprite = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/sprites/levels/level_" + PlayerPrefs.GetInt("Level").ToString("00") + ".png", typeof(Texture2D));
        _levelSprite = Resources.Load<Texture2D>("levels/level_" + PlayerPrefs.GetInt("Level").ToString("00"));
        _map = GameObject.Find("map").transform;
        Brick.BricksCount = 0;

        SetBackground();
        GenerateMap();
    }

    void SetBackground()
    {
        var p_level = PlayerPrefs.GetInt("Level");

        var bg_number = p_level;
        // if (p_level <= 3)
        // {
        //     bg_number = 1;
        // }
        // else if (p_level <= 6) bg_number = 2;
        // else if (p_level <= 9) bg_number = 3;
        // else if (p_level <= 12) bg_number = 4;
        // else if (p_level <= 14) bg_number = 5;
        // else bg_number = 6;

        var path = "background/background_" + bg_number.ToString();
        var bg_renderer = GameObject.Find("stage").transform.Find("background").GetComponent<SpriteRenderer>();
        bg_renderer.sprite = Resources.Load<Sprite>(path);
    }

    void GenerateMap()
    {
        var xIsEven = _levelSprite.width % 2 != 0;

        for (var y = 0; y < _levelSprite.height; y++)
        {
            for (var x = 0; x < _levelSprite.width / 2; x++)
            {
                if (xIsEven && x == 0)
                {
                    var pos = _levelSprite.width / 2;
                    var pixel = _levelSprite.GetPixel(pos, y);

                    GenerateBrick(pixel, new Vector2(pos, y));
                    continue;
                }

                var leftPos = _levelSprite.width / 2 - x - 1;
                var worldLeftPos = -x - .5f;
                var leftPixel = _levelSprite.GetPixel(leftPos, y);
                GenerateBrick(leftPixel, new Vector2(worldLeftPos, y));
                
                var rightPos = _levelSprite.width / 2 + x + (xIsEven ? 1 : 0);
                var worldRightPos = x + .5f;
                var rightPixel = _levelSprite.GetPixel(rightPos, y);
                GenerateBrick(rightPixel, new Vector2(worldRightPos, y));
            }
        }
    }

    void GenerateBrick(Color pixelColor, Vector3 position)
    {
        if (pixelColor.a == 0) return;

        var mapper = Mapping.FirstOrDefault(_ => _.Color.Equals(pixelColor));
        if (mapper == null)
        {
            Debug.LogError("Mapper não encontrado. Você está fazendo isto errado. \nr:" + pixelColor.r + " g:" +
                           pixelColor.g + "b:" + pixelColor.b);
            return;
        }

        var prefab = mapper.Prefab;

        var brick = Instantiate(prefab, _map, true);
        brick.transform.localPosition = position;
    }

    [System.Serializable]
    public class ColorToPrefab
    {
        public Color Color;
        public GameObject Prefab;
    }
}