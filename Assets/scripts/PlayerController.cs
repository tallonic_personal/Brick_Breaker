﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float MoveSpeed = 5f;
    
    void Update()
    {
        if (SceneController.paused) return;
        
        var worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        worldPosition.x = Mathf.Clamp(worldPosition.x, -13, 13);
        worldPosition.y = transform.position.y;
        worldPosition.z = 0f;
        transform.position = worldPosition;
    }

    public Sprite GetSprite()
    {
        return GetComponent<SpriteRenderer>().sprite;
    }
}
