﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Brick : MonoBehaviour
{
    public List<Sprite> sprites;
    public static int BricksCount;
    public SpriteRenderer breakSprite;

    int _hits = 0;
    SceneController _sceneController;

    void Awake()
    {
        bool isBreakable = tag == "Breakable";
        if (isBreakable)
            BricksCount++;

        _sceneController = FindObjectOfType<SceneController>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        bool isBreakable = tag == "Breakable";
        if (isBreakable)
            HandleHits();
    }

    void HandleHits()
    {
        _hits++;
        var maxHits = sprites.Count;

        if (_hits >= maxHits)
        {
            StartCoroutine(DestroyAnimation());
            BricksCount--;
            GetComponent<Collider2D>().enabled = false;
            _sceneController.CheckBricksDestroyed();
        }
        else
        {
            StartCoroutine(PopAnimation());
            LoadSprites();
        }
    }

    private IEnumerator DestroyAnimation()
    {
        yield return transform.DOScale(Vector3.one * 1.2f, .1f).WaitForCompletion();
        yield return transform.DOScale(Vector3.zero, .4f).WaitForCompletion();

        Destroy(gameObject);
    }

    private IEnumerator PopAnimation()
    {
        yield return transform.DOScale(Vector3.one * 1.2f, .1f).WaitForCompletion();
        yield return transform.DOScale(Vector3.one, .1f).WaitForCompletion();
    }

    void LoadSprites()
    {
        var spriteIndex = _hits;
        if (sprites[spriteIndex])
            breakSprite.sprite = sprites[spriteIndex];
    }
}